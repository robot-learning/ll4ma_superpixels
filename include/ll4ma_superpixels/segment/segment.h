#include <opencv2/core/core.hpp>
namespace ll4ma_superpixels
{
// RGB only
cv::Mat getSuperpixelImage(cv::Mat input_img, int& num_ccs,
                           cv::Mat& display_img, double sigma=0.3,
                           double k=500.0, int min_size=10);
// RGB + Depth
cv::Mat getSuperpixelImage(cv::Mat color_img, cv::Mat depth_img, int& num_ccs,
                           cv::Mat& display_img, double sigma=0.3,
                           double k=500.0, int min_size=10, double wr=0.1,
                           double wg=0.1, double wb=0.1, double wd=0.7);

// RGB + Depth + Surface Normals
cv::Mat getSuperpixelImage(cv::Mat color_img, cv::Mat depth_img, cv::Mat normal_img,
                           int& num_ccs, cv::Mat& display_img, double sigma=0.3,
                           double k=500.0, int min_size=10, double wr=0.1,
                           double wg=0.1, double wb=0.1, double wd=0.7,
                           double wn=0.1);

};
