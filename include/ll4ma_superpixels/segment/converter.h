/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "image.h"
#include <opencv2/opencv.hpp>

image<rgb>* MATtoFELZS(cv::Mat input)
{
    image<rgb> *output;
    output = new image<rgb>(input.cols, input.rows, true);
    double rColor, gColor, bColor;

    for(int y = 0; y < input.rows; ++y) {
        for(int x = 0; x < input.cols; ++x) {
            bColor = input.at<cv::Vec3b>(y,x)[0];
            gColor = input.at<cv::Vec3b>(y,x)[1];
            rColor = input.at<cv::Vec3b>(y,x)[2];
            output->data[y * output->width() + x].b = bColor;
            output->data[y * output->width() + x].g = gColor;
            output->data[y * output->width() + x].r = rColor;
        }
    }

    return output;
}

cv::Mat FELZStoMAT(image<rgb>* input)
{
    cv::Mat output(input->height(), input->width(), CV_8UC3);

    for(int i = 0; i < output.rows; ++i) {
        for(int j = 0; j < output.cols; ++j) {
            rgb val = imRef(input, j, i);
            output.at<cv::Vec3b>(i,j)[0] = val.b;
            output.at<cv::Vec3b>(i,j)[1] = val.g;
            output.at<cv::Vec3b>(i,j)[2] = val.r;
        }
    }

    return output;
}

cv::Mat FELZSIDXtoMAT(image<rgb>* input)
{
  cv::Mat output(input->height(), input->width(), CV_8UC1);
  for(int i = 0; i < output.rows; ++i) {
    for(int j = 0; j < output.cols; ++j) {
      output.at<uchar>(i,j) = imRef(input, j, i).idx;
    }
  }
  return output;
}

image<float>* DEPTHtoFELZS(cv::Mat& input)
{
  image<float> *output;
  output = new image<float>(input.cols, input.rows, true);
  for(int y = 0; y < input.rows; ++y) {
    for(int x = 0; x < input.cols; ++x) {
      output->data[y * output->width() + x] = input.at<float>(y,x);
    }
  }
  return output;
}

cv::Mat FELZSDEPTHtoMAT(image<float>* input)
{
  cv::Mat output(input->height(), input->width(), CV_32FC1);
  for(int i = 0; i < output.rows; ++i) {
    for(int j = 0; j < output.cols; ++j) {
      output.at<float>(i, j) = imRef(input, j, i);
    }
  }
  return output;
}
