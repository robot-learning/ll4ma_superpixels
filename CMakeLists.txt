cmake_minimum_required(VERSION 2.8.3)
project(ll4ma_superpixels)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(catkin REQUIRED COMPONENTS
  roscpp
  message_generation
  genmsg
  std_msgs
  sensor_msgs
  pcl_conversions
  pcl_ros

)

find_package(OpenCV REQUIRED)
find_package(PCL 1.7 REQUIRED COMPONENTS common io)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
add_service_files(
  FILES
  SmoothClutter.srv
  )

generate_messages(
  DEPENDENCIES
  std_msgs
  sensor_msgs
)

catkin_package(
  CATKIN_DEPENDS
  roscpp
  std_msgs
  sensor_msgs
  INCLUDE_DIRS include
  LIBRARIES cpl_superpixels_base
  )

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
  )
#common commands for building c++ executables and libraries
add_library(cpl_superpixels_base src/segment.cpp)
target_link_libraries(cpl_superpixels_base ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

# Simple test executable
add_executable(test_superpixels src/test_superpixels.cpp)
target_link_libraries(test_superpixels cpl_superpixels_base ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

# Smoothing node for noisy label inputs
# add_executable(superpixel_smoother_node src/superpixel_smoother_node.cpp)
# target_link_libraries(superpixel_smoother_node ${catkin_LIBRARIES})

# Offline version of the above
# add_executable(offline_clutter_test_node src/offline_clutter_test_node.cpp)
# target_link_libraries(offline_clutter_test_node ${catkin_LIBRARIES})
