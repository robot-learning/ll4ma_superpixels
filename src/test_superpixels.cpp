// #include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <iostream>
#include <ll4ma_superpixels/segment/segment.h>
// #include <pcl/io/io.h>
// #include <pcl/io/pcd_io.h>
// #include <pcl/features/integral_image_normal.h>

int main(int argc, char** argv)
{
  if (argc < 2)  {
    std::cout << "usage: " << std::string(argv[0]) << " color_img depth_img" << std::endl;
    return -1;
  }
  std::cout << "Input image name: " << std::string(argv[1]) << std::endl;
  // Read in image from file
  cv::Mat input_img = cv::imread(argv[1]);
  if (input_img.rows < 1) return -1;

  // Optionally read in depth image from file
  cv::Mat depth_img_raw = cv::imread(argv[2], cv::IMREAD_GRAYSCALE);
  cv::Mat depth_img(depth_img_raw.rows, depth_img_raw.cols, CV_32FC1, 0.0);

  // hack: Just use an img of all 0s as the normals to test the code runs; need to
  // complete the TODOs below (or read in PCD directly) to test the semantics
  // cv::Mat normal_img(depth_img_raw.rows, depth_img_raw.cols, CV_32FC1, 0.0);
  bool have_depth = false;
  if (depth_img.rows > 1) {
    have_depth = true;
    // Depth expects floating point values
    std::cout << "Convertin depth image" << std::endl;
    depth_img_raw.convertTo(depth_img, CV_32FC1);
    // TODO: Convert depth_img to point cloud
    // TODO: estimate surface normals for point cloud
    // TODO: Convert normals back into an image
  }

  // TODO: Optionally read these values in from command line
  double sigma=0.5;
  double k=350.0;
  int min_size=1000;

  // Run superpixel segmentation on the image(s)
  int num_regions = 0;
  cv::Mat display_img, display_img_depth, display_img_norms, label_img;
  label_img = ll4ma_superpixels::getSuperpixelImage(input_img, num_regions,
                                                    display_img, sigma, k, min_size);
  std::cout << "Got " << num_regions << " regular superpixels" << std::endl;
  if (have_depth)
  {
    label_img = ll4ma_superpixels::getSuperpixelImage(input_img, depth_img, num_regions,
                                                      display_img_depth, sigma, k, min_size);
    std::cout << "Got " << num_regions << " depth superpixels" << std::endl;
    // // TODO: Test with normals here
    // label_img = ll4ma_superpixels::getSuperpixelImage(input_img, depth_img, normal_img,
    //                                                   num_regions, display_img_norms,
    //                                                   sigma, k, min_size);

    // std::cout << "Got " << num_regions << " depth+normal superpixels" << std::endl;
  }

  // Display results with high_gui

  cv::imshow("Input image", input_img);
  cv::imshow("Superpixels", display_img);
  if (have_depth)
  {
    cv::imshow("Input depth", depth_img);
    cv::imshow("Input depth raw scaled", depth_img_raw);
    cv::imshow("Superpixels_depth", display_img_depth);
    // cv::imshow("Superpixels_norms", display_img_norms);
  }
  cv::imshow("label_img", label_img);
  cv::waitKey();

  // std::cout << "label_img channels " << label_img.channels() << std::endl;
  // std::cout << "label_img type " << label_img.type() << std::endl;
  // std::cout << "label_img depth " << label_img.depth() << std::endl;

  // Save data to disk
  // cv::imwrite("~/sandbox/superpixels_out.png", label_img);

  return 0;
}
