#include <ros/ros.h>
#include "ll4ma_superpixels/segment/segment.h"
#include <opencv2/highgui/highgui.hpp>
#include <string>

#include "ll4ma_superpixels/segment/segment-image.h"
#include "ll4ma_superpixels/segment/image.h"
#include "ll4ma_superpixels/segment/converter.h"

namespace ll4ma_superpixels
{

cv::Mat getSuperpixelImage(cv::Mat input_img, int& num_ccs, cv::Mat& disp_img,
                           double sigma, double k, int min_size)
{
  // Superpixels, Felzenszwalb
  image<rgb>* im = MATtoFELZS(input_img);
  image<rgb> *disp_im = segment_image(im, sigma, k, min_size, &num_ccs);
  delete im;
  disp_img = FELZStoMAT(disp_im);
  cv::Mat idx_img;
  idx_img = FELZSIDXtoMAT(disp_im);
  delete disp_im;
  return idx_img;
}

cv::Mat getSuperpixelImage(cv::Mat color_img, cv::Mat depth_img, int& num_ccs,
                           cv::Mat& disp_img, double sigma, double k,
                           int min_size,
                           double wr, double wg, double wb, double wd)
{
  // Superpixels, Felzenszwalb
  image<rgb>* color_im = MATtoFELZS(color_img);
  image<float>* depth_im = DEPTHtoFELZS(depth_img);
  image<rgb> *disp_im = segment_image(color_im, depth_im, sigma, k, min_size,
                                      &num_ccs, wr, wg, wb, wd);
  delete color_im;
  delete depth_im;

  cv::Mat idx_img;
  disp_img = FELZStoMAT(disp_im);
  idx_img = FELZSIDXtoMAT(disp_im);
  delete disp_im;
  return idx_img;
}

cv::Mat getSuperpixelImage(cv::Mat color_img, cv::Mat depth_img, cv::Mat normal_img,
                            int& num_ccs, cv::Mat& display_img, double sigma,
                            double k, int min_size, double wr,
                            double wg, double wb, double wd,
                            double wn)
{
  // Superpixel generation given normal vector
  // Superpixels, Felzenszwalb
  image<rgb>* color_im = MATtoFELZS(color_img);
  image<rgb>* normal_im = MATtoFELZS(normal_img);
  image<float>* depth_im = DEPTHtoFELZS(depth_img);
  cv::Mat depth_revert;
  image<rgb> *disp_im = segment_image(color_im, depth_im, normal_im,
                                      sigma, k, min_size, &num_ccs,
                                      wr, wg, wb, wd, wn);
  delete color_im;
  delete normal_im;
  delete depth_im;
  // Convert to cv::Mat
  cv::Mat idx_img;
  display_img = FELZStoMAT(disp_im);
  idx_img = FELZSIDXtoMAT(disp_im);
  return idx_img;
}

};
